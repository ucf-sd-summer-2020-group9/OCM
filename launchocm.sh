#!/bin/bash

/usr/bin/mysqld_safe &
/usr/sbin/httpd 
sleep 10

RESULT=`mysql -uroot -sN -e "SHOW DATABASES LIKE 'cms'"`
if [ "$RESULT" == "cms" ]; then
  echo "Database already exists"
else
  echo "Database does not exist"
  mysql -uroot -e "create database cms"
  cat /var/www/html/cms/app/sql/install/new_install.sql | mysql -uroot cms 
  mysql -uroot -e "update users set username='support', password=md5('password')" cms
  cp /var/www/html/cms-custom/config/settings.php.example /var/www/html/cms-custom/config/settings.php
fi



tail -f /dev/null
