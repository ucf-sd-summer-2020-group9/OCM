# OCM

### What is OCM? ###

Open Case Management (OCM) is an extensible, modern, customizable, mobile-friendly and browser-based case management systems engineered to meet the needs of non-profit legal aid organizations, as well as other human services non-profits. Development is underway to add features to OCM that are desirable for private practice.

OCM is free, open source software. Version 8 is available at https://gitlab.com/amsclark/OCM. Some free-standing modules are available as well among Alex Clark of Metatheria’s github repositories: https://github.com/amsclark?tab=repositories. The release of version 8 as a major forked version is scheduled for early 2020. The original branch and version 7 of OCM is available at https://github.com/aworley/ocm.

Documentation for OCM is available through a wiki at https://github.com/aworley/ocm/wiki as well as in the form of some training videos which are being posted as they are recorded at http://metatheria.solutions/ocm-training/.

### Installing OCM ###

OCM requires specific versions of PHP and database software, some of which are newer than the standard versions of these packages included within standard GNU/Linux package system repositories for popular GNU/Linux distributions. Consequently, it is encouraged that setup be performed using the included docker-compose.yml file. Please note that the default credentials for the application appear within line 14 of launchocm.sh and can be changed there before running docker-compose. 

Free help with installation is available upon request by Metatheria within the [OCM Slack server](https://join.slack.com/t/opencasemanagement/shared_invite/enQtOTcyMTEyMDY1ODQ0LTcyMjRlZDJkODQ4OTg3ZjBiMjZmYTk3NmRmZjI4M2M1ZmIwMGUyMzAxNmMyNGY0NDBiYzZjNjExN2Q1OGE0Mjg). Additionally, paid managed hosting and support services for OCM are available through Metatheria. Pricing details for these services is available at http://metatheria.solutions/ocm/ 

### Post-Install Setup ###

OCM will look very strange the first time you log in because it needs several URLs within the /cms/system-settings.php settings page to be set to static resources for Font Awesome, Bootstrap, and jQuery CDNs. 

If you are setting up OCM for use with tight integration with Docassemble, review the explanations at https://docassemble.org/docs/interviews.html#iframe to find the /test\_embed endpoint for your Docassemble server. Use the static resource URLs between the HEAD tags of your /test\_embed within the system settings screen of OCM.

If you are not going to be integrating OCM with Docassemble, you can use these URLS within /cms/system-settings.php:


fontawesome script: https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css
bootstrap: https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css
other jquery: https://code.jquery.com/jquery-3.4.1.min.js

After setup you will also want to set up backups and an SSL certificate. Currently, this will have to be done within the container as a way to do so within the administrative interface of OCM has not yet been developed.