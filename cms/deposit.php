<?php

require_once 'app/lib/Template.php';
require_once 'app/lib/Grant.php';

require_once('pika-danio.php');
pika_init();

require_once('pikaMisc.php');

$user_id = $auth_row['user_id'];

$grant = new Grant();

$grant_id = isset($_GET['id']) ? $_GET['id'] : null;

if (pl_grab_post('deposit-submit'))
{
    $data = array();

    $data['date_of_service'] = pl_grab_post('date_of_service', 'date');
    $data['time_spent'] = pl_grab_post('amount');
    $data['notes'] = pl_grab_post('notes');
    $data['new_hours'] = pl_grab_post('running_total') + $data['time_spent'];

    if ($grant->deposit($user_id, $grant_id, $data))
    {
        $alert = '<div class="alert alert-success" role="alert">
                    Deposited to grant.
                  </div>';
    }
    else
    {
        trigger_error('Could not deposit to grant.');
    }
}

$template = new Template('subtemplates/grant-deposit.php');

$template->base_url = pl_settings_get('base_url');
$template->branding = pl_settings_get('branding');
$template->owner_name = pl_settings_get('owner_name');
$template->reports = pikaMisc::reportList(true);
$template->grant = $grant->getSingleGrant($grant_id);
$template->alert = $alert;

echo $template;