<?php

/******************
 * 
 * This sends html
 * 
 ******************/

require_once 'app/lib/Template.php';
require_once 'app/lib/Grant.php';

require_once('pika-danio.php');
pika_init();

require_once('pikaMisc.php');

$user_id = $auth_row['user_id'];

$grant = new Grant();

// could probably make some helper class for this?
if (pl_grab_get('error'))
{
    $error = pl_grab_get('error');
    switch ($error)
    {
        case 'emptyfields':
            $message = "Fill out all required fields.";
            break;
        case 'invalidfundingcode':
            $message = "Funding code already exists in system.";
            break;
        default:
            $message = "Error";
            break;
    }

    $alert = '<div class="alert alert-warning" role="alert">' .
                $message
            . '</div>';
}

if (pl_grab_post('grant-submit'))
{
    $data = array();

    $data['office_id'] = pl_grab_post('office_id');
    $data['funding_code'] = pl_grab_post('funding_code');
    $data['source'] = pl_grab_post('source');
    $data['description'] = pl_grab_post('description');
    $data['hours'] = pl_grab_post('hours');
    $data['start_date'] = pl_grab_post('start_date', 'date');
    $data['end_date'] = pl_grab_post('end_date', 'date');
    $data['is_active'] = pl_grab_post('is_active');

    if (!$data['source'] || !$data['start_date'] || !isset($data['is_active']) || !$data['funding_code'])
    {
        header('Location: create-grant.php?error=emptyfields');
        exit;
    }

    // check if funding code already exists
    if ($grant->checkFundingCode($data['funding_code']))
    {
        header('Location: create-grant.php?error=invalidfundingcode');
        exit;
    }

    $grant->createGrant($user_id, $data);
    $alert = '<div class="alert alert-success" role="alert">
                Grant created.
              </div>';
}

$template = new Template('subtemplates/grant-create.php');

$template->base_url = pl_settings_get('base_url');
$template->branding = pl_settings_get('branding');
$template->owner_name = pl_settings_get('owner_name');
$template->reports = pikaMisc::reportList(true);
$template->offices = $grant->getAllOffices();
$template->funding_codes = $grant->getFundingCodes();
$template->alert = $alert;

echo $template;
