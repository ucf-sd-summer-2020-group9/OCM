<?php
/***********************************/
/* Pika CMS (C) Pika Software      */
/* http://pikasoftware.com         */
/*                                 */
/* Modified January 2020           */
/* By Metatheria, LLC              */ 
/* https://metatheria.solutions    */
/***********************************/

function red_flag($field_name = null, $field_value = null, $menu_array = null, $args = null)
{
	$flag_output = '';
	if(!is_null($field_value) && !is_array($field_value) && $field_value) {
	
		$field_value = str_replace(' ', '&nbsp;', $field_value);
		
		$flag_output .=	"<span class=\"badge badge-pill badge-danger\"><span class=\"fas fa-exclamation\"></span>&nbsp;  {$field_value}</span>";
		
		
	}
	
	return $flag_output;
}

?>
