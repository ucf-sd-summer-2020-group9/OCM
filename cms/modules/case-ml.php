<?php
/************************************/
/* Authored February 2020           */
/* By Metatheria, LLC               */ 
/* https://metatheria.solutions     */
/*                                  */
/* this file is free and            */
/* unencumbered software released   */
/* into the public domain under the */
/* terms of the Unlicense.          */
/*                                  */
/* https://unlicense.org            */
/************************************/

$da_template = new pikaTempLib('subtemplates/case-ml.html',$case_row);
$C .= $da_template->draw();
