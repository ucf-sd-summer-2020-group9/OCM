<?php

chdir('../');

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once 'app/lib/Grant.php';

// Pika Authentication
require_once 'pika-danio.php';
pika_init();

$grant = new Grant;

$result = $grant->getAllBillingTypes();

header('HTTP/1.1 200 OK');
echo json_encode($result);