<?php

/**********************************
 * 
 * 
 * 
 * 
 * 
 **********************************/

require_once 'app/lib/Template.php';
require_once 'app/lib/Grant.php';

require_once('pika-danio.php');
pika_init();

require_once('pikaMisc.php');

$user_id = $auth_row['user_id'];

$grant = new Grant;

$grant_id = isset($_GET['id']) ? $_GET['id'] : null;

if (isset($_POST['download-id']))
{
    $download_id = $_POST['download-id'];
    $granty_grant = $grant->getSingleGrant($download_id);

    // these headers tell the browser that 
    // ..we're downloading a .csv file.
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=grant' . $download_id . '-transactions.csv');

    $fp = fopen('php://output', 'w');

    fputcsv($fp, array($granty_grant['source']));

    // columns
    fputcsv($fp, array(
        'Transaction ID', 
        'User ID', 
        'User First Name', 
        'User Last Name', 
        'Case ID',
        'Case Number',
        'Caseworker ID',
        'Caseworker First Name',
        'Caseworker Last Name',
        'Date of Service',
        'Work Activity',
        'System Timestamp',
        'Time Spent (hrs)', 
        'System Notes' 
    ));

    $transactions = $grant->getTransactions($download_id);

    foreach ($transactions as $transaction)
    {
        fputcsv($fp, $transaction);
    }

    fputcsv($fp, array(
        'Running Total',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        $granty_grant['hours']
    ));
    fclose($fp);
    exit;
}

$template = new Template('subtemplates/transactions-list.php');

$template->grant = $grant->getSingleGrant($grant_id);
$template->transactions = $grant->getTransactions($grant_id);
$template->base_url = pl_settings_get('base_url');
$template->branding = pl_settings_get('branding');
$template->owner_name = pl_settings_get('owner_name');
$template->reports = pikaMisc::reportList(true);

echo $template;