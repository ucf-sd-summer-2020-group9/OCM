<?php

require_once 'app/lib/Template.php';
require_once 'app/lib/Grant.php';

require_once('pika-danio.php');
pika_init();

require_once('pikaMisc.php');

$grant = new Grant();

$template = new Template('subtemplates/grant-list.php');

$template->base_url = pl_settings_get('base_url');
$template->branding = pl_settings_get('branding');
$template->owner_name = pl_settings_get('owner_name');
$template->grants = $grant->getAllGrants($auth_row['read_all'], $auth_row['read_office']);
$template->reports = pikaMisc::reportList(true);

echo $template;

?>

<!--
<div id='root'></div>

<script  type="text/babel">

class App extends React.Component {
  state = {
    grants: []
  }

  componentDidMount() {
    const url = 'ops/grants.php'
    axios.get(url).then(response => response.data)
    .then((data) => {
      this.setState({ grants: data })
      console.log(this.state.grants)
     })
  }

  render() {
    return (
        <React.Fragment>
        <h3>Grant Management</h3>
        <br></br>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Source</th>
                    <th scope="col">Funding Code</th>
                    <th scope="col">Running Total</th>
                    <th scope="col">Is Active</th> 
                </tr>
            </thead>
        {this.state.grants.map((grant) => (
        <tbody>
            <tr>
                <td scope="row">{ grant.source }</td>
                <td scope="row">{ grant.funding_code }</td>
                <td scope="row">{ grant.running_total }</td>
                <td scope="row">{ grant.is_active }</td>
                <td><a class="btn btn-outline-secondary" href="grants.php?id=#">Edit</a></td>
            </tr>
        </tbody>
        ))}
        </table>
        </React.Fragment>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
</script>
</body>
-->