<?php
/************************************/
/* Authored December 2019           */
/* By Metatheria, LLC               */ 
/* https://metatheria.solutions     */
/*                                  */
/* resetpw.php is free and          */
/* unencumbered software released   */
/* into the public domain under the */
/* terms of the Unlicense.          */
/*                                  */
/* https://unlicense.org            */
/************************************/

// We want pika-danio.php classes and functions to be available and want to use Pika's database connection, but we also don't want the user to be forced to authenticate since this is a pw reset module.
define('PL_DISABLE_SECURITY', true);
require_once('pika-danio.php');

pika_init();

//token generation function
$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
function generate_string($input, $strength = 50) {
    $input_length = strlen($input);
    $random_string = '';
    for($i = 0; $i < $strength; $i++) {
        $random_character = $input[mt_rand(0, $input_length - 1)];
        $random_string .= $random_character;
    }

    return $random_string;
}

echo "<!DOCTYPE html>";
echo "<html lang=\"en\">";
echo "<head>";
echo "  <title> " . pl_settings_get('branding') . " | Reset Lost/Expired Password</title>";
echo "  <meta charset=\"utf-8\">";
echo "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">";
echo "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\">";
echo "  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>";
echo "  <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\"></script>";
echo "  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\"></script>";
echo "  <link rel=\"stylesheet\" href=\"css/custom.css\">";
echo "</head>";
echo "<body class=\"bg-purple\">";
echo "<div class=\"jumbotron jumbotron-fluid\">
      <div class=\"container neumorphism-jumbotron\">
        <div style=\"padding: 25px\">
          <h1 class=\"display-1\">" . pl_settings_get('branding') . ".</h1>
          <p class=\"lead\">" . pl_settings_get('owner_name') . ".</p>
        </div>
      </div>
    </div>";
echo "  <div class=\"container\"><div class=\"row\"><div class=\"col-md-12 d-flex flex-column\"><div class=\"row\"><div class=\"col-lg-12 col-md-12\"><div class=\"neumorphism-card\"><div style=\"padding: 75px\">";
echo "<h2>" . pl_settings_get('branding') . " Password Reset</h2>";

if (strlen(pl_settings_get('sparkpost_api_key')) == 0) {
die('password self-reset not configured on this server. please set sparkpost api key from setting screen');
}


if (isset($_POST['username'])){
  $safe_username=DB::escapeString($_POST['username']);
  $sql = "select user_id, username, email from users where username='" . $safe_username . "'";
  $result = DB::query($sql) or trigger_error("SQL: " . $sql . " ERROR: " . DB::error());
  
  if (DBResult::numRows($result) == 1) {
    //echo "<pre>";
    while ($row = DBResult::fetchRow($result))
    { 
      $emailFromDB = $row['email'];
    }
      //clean any old tokens for user from the db
      $clean_sql = "DELETE FROM pw_reset_tokens where username='" . $safe_username . "'";
      DB::query($clean_sql) or trigger_error("SQL: " . $clean_sql . " ERROR: " . DB::error());
      //create a new token for the user
      $resetToken = generate_string($permitted_chars, 50);
      $token_sql = "INSERT INTO pw_reset_tokens (username, token, token_expire) VALUES ('" . $safe_username . "', '" . $resetToken . "', '" . strtotime( '+3600 seconds' ) . "')"; // 3600 corresponds to the number of seconds in one hour. Change this for a different interval of time that the reset tokens should stay good for. 
      DB::query($token_sql) or trigger_error("SQL: " . $token_sql . " ERROR: " . DB::error());
      //email the user their reset link
        send_reset_email(DB::escapeString($emailFromDB),'Forgotten Password','Please visit https://' . $_SERVER['SERVER_NAME'] . pl_settings_get('base_url') . "/resetpw.php?token=" . $resetToken . ' to reset your password'); 
        echo "Please check your email. The password reset link was sent to $emailFromDB";
  } 
  else {
  echo "Invalid username. Please try again.";
  echo "<form method=\"POST\" name=\"resetpw\"> Please enter your username.<br>";
  echo "<div class=\"input-group\">";
  echo "<input class=\"form-control\" type=\"text\" name=\"username\" id=\"username\">";
  echo "<div class=\"input-group-append\">";
  echo "<button class=\"btn btn-success\" type=\"submit\">Submit</button>";
  echo "</div>";
  echo "</div>";
  echo "</form>"; 
  echo "</div></div></div></div></div></div>";
  }
}
else {
  if (!isset($_POST['username']) && !isset($_POST['token']) && isset($_GET['token'])) {
    $safe_token = DB::escapeString($_GET['token']);
    $token_search_sql = "select username, token, token_expire from pw_reset_tokens where token='" . $safe_token  . "' and token_expire > " . strtotime("now");
    $token_search_result = DB::query($token_search_sql) or trigger_error("SQL: " . $token_search_sql . " ERROR: " . DB::error());
    if (DBResult::numRows($token_search_result) == 1) {
      while ($row = DBResult::fetchRow($token_search_result)) {
        $forgetful_user = $row['username'];
      }
      $temppass = generate_string($permitted_chars, 14);
      $reset_sql ="update users set password='";
      $reset_sql .= md5($temppass);
      $reset_sql .= "', password_expire='";
      $reset_sql .= strtotime('+660 seconds'); // 11 minutes
      $reset_sql .= "' where username='";
      $reset_sql .= $forgetful_user;
      $reset_sql .= "'";
      DB::query($reset_sql) or trigger_error("SQL: " . $reset_sql . " ERROR: " . DB::error());
      echo "Your password has been temporarily reset to <strong>$temppass</strong>. Please click <a href=\"password.php\" target=\"_blank\" style=\"color: #d49239\">here</a> to log in with this temporary password and set a password of your choice. Please note that when the password change screen asks for your \"Current Password\" it is asking for <strong>$temppass</strong>. Your temporary password of <strong>$temppass</strong> will expire in 10 minutes, so please change your password now.";
      }
    else {
        echo "The password reset link is either invalid or expired. ";
      }
  }
  else {
    echo "<form method=\"POST\" name=\"resetpw\"> Please enter your username.<br>";
    echo "<div class=\"input-group\">";
    echo "<input class=\"form-control\" type=\"text\" name=\"username\" id=\"username\">";
    echo "<div class=\"input-group-append\">";
    echo "<button class=\"btn btn-success\" type=\"submit\">Submit</button>";
    echo "</div>";
    echo "</div>";
    echo "</form>";
    echo "</div></div></div></div></div></div>";
  }
}

echo "</div>";



function send_reset_email($forgetfuluser_email, $subject, $reject_message){

  $data_string='{"options": {"sandbox": false, "open_tracking": false, "click_tracking": false}, "content": {"from": "' 
                        . pl_settings_get('sparkpost_from_address') 
                        . '", "subject": "' . $subject . '", "text":"' . $reject_message 
                        . '"}, "recipients": [{"address": "' . $forgetfuluser_email . '"}]}';
//var_dump($data_string);
                $c = curl_init();
                curl_setopt($c, CURLOPT_URL, 'https://api.sparkpost.com/api/v1/transmissions');
                curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($c, CURLOPT_TIMEOUT, 30);
                curl_setopt($c, CURLOPT_RETURNTRANSFER, 1); 
                curl_setopt($c, CURLOPT_SSLVERSION, 6); 
                curl_setopt($c, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($c, CURLOPT_HTTPHEADER, array(
                                            'Content-Type: application/json',
                                            'Authorization: ' . pl_settings_get('sparkpost_api_key')
                                            )); 
                //$status_code = curl_getinfo($c, CURLINFO_HTTP_CODE);
                $exit_code = curl_exec($c);
                curl_close ($c);
                $exit_array = json_decode($exit_code);
                //var_dump($exit_array);
                return $exit_array->total_accepted_recipients;
}
