<?php include 'inc/header.php'; ?>

<h3 class="page-header">Edit Grant  <small class="text-muted"><?= $grant['source']; ?></small></h3>
	<form method="post" action="edit-grant.php?id=<?= $grant['grant_id']; ?>">
		<div class="form-group">
			<label>Office*</label>
			<select class="form-control" name="office_id">
            <?php foreach ($offices as $office): ?>
                <option <?= $office['value'] == $grant['office_id'] ? 'selected' : ''; ?> value="<?= $office['menu_order']; ?>"><?= $office['label']; ?></option>
			<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group">
			<label>Funding Code*</label>
			<input type="text" class="form-control" name="funding_code" value="<?= $grant['funding_code']; ?>">
		</div>
		<div class="form-group">
			<label>Source*</label>
			<input type="text" class="form-control" name="source" value="<?= $grant['source']; ?>">
		</div>
		<div class="form-group">
			<label>Description</label>
			<textarea class="form-control" name="description"><?= $grant['description']; ?></textarea>
		</div>
		<div class="form-group">
			<label>Start Date*</label>
			<input type="date" class="form-control" name="start_date" value="<?= $grant['start_date']; ?>">
		</div>
		<div class="form-group">
			<label>End Date</label>
			<input type="date" class="form-control" name="end_date" value="<?= $grant['end_date']; ?>">
		</div>
        <div class="form-group">
			<label>Active*</label>
			<input type="radio" id="active" name="is_active" value=1 <?= $grant['is_active'] ? 'checked' : '' ?>>
        	<label for="active">Yes</label>
        	<input type="radio" id="inactive" name="is_active" value=0 <?= $grant['is_active'] ? '' : 'checked' ?>>
        	<label for="inactive">No</label>
		</div>
		<input type="submit" class="btn btn-success" value="Submit" name="edit-submit">
	</form>

<?php include 'inc/footer.php'; ?>