<!DOCTYPE html>
<!-- default.html - Pika CMS (C) Pika Software |  http://pikasoftware.com // Modified December 2019 By Metatheria, LLC | https://metatheria.solutions -->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="%%[shortcut_icon]%%">
    <link rel="apple-touch-icon" sizes="180x180" href="%%[apple_touch_icon]%%">
    <link rel="icon" type="image/png" href="%%[icon32x32]%%" sizes="32x32">
    <link rel="icon" type="image/png" href="%%[icon16x16]%%" sizes="16x16">
    <link rel="manifest" href="%%[manifest_setting]%%">
    <link rel="mask-icon" href="%%[mask_icon]%%" color="#698aa7">
    <meta name="theme-color" content="#83b3dd">
    <script defer src="%%[fontawesome]%%"></script>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="%%[da_bundle_css]%%" rel="stylesheet">
    <link href="%%[da_playground_css]%%" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="./css/custom.css">

    <title><?= $branding; ?> | <?= $owner_name; ?></title>
    
<!--
    <script src= "https://unpkg.com/react@16/umd/react.production.min.js"></script>
    <script src= "https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
    <script src="https://unpkg.com/babel-standalone@6.15.0/babel.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
-->

</head>

<body>
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3><?= $owner_name; ?></h3><div class="dropdown-divider"></div><h5><?= $branding; ?></h5>
            </div>
            <ul class="list-unstyled components">
                <li id="dashboard-nav-item">
                  <a href=<?= $base_url; ?>>Dashboard</a>
                </li>
                <li id="caselist-nav-item">
                  <a href="<?= $base_url; ?>/case_list.php?mode=open">Cases</a>
                </li>
                <li id="calendar-nav-item">
                  <a href="#calendarSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Calendar</a>
                  <ul class="collapse list-unstyled" id="calendarSubmenu">
                    <li><a href="<?= $base_url; ?>/cal_day.php">Day View</a></li>
                    <li><a href="<?= $base_url; ?>/cal_week.php?screen=one">Week View</a></li>
                    <li><a href="<?= $base_url; ?>/cal_week.php?screen=four">Four Week View</a></li>
                    <li><a href="<?= $base_url; ?>/cal_adv.php">Advanced View</a></li>
                    <li><a href="<?= $base_url; ?>/activity.php?screen=compose&act_type=C">Quick Calendar Entry</a></li>
                  </ul>
                </li>
                 <li id="tickler-nav-item">
                  <a href="<?= $base_url; ?>/activity.php?screen=compose&act_type=K">New Tickler</a>
                </li>
                <li id="intake-nav-item">
                  <a href="<?= $base_url; ?>/intake2.php">New Intake</a>
                </li>
                <li id="transfers-nav-item">
                  <a href="transfers.php">Transfers</a>
                </li>
                <li id="grants-nav-item">
                  <a href="./grants.php">Grants</a>
                </li>
	        <li id="reports-nav-item">
                    <a href="#reportSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Reports</a>
                    <ul class="collapse list-unstyled" id="reportSubmenu">
                        <?php foreach($reports as $report): ?>
                          <li><?= $report; ?></li>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <li>
                  <a href="#settingsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Settings</a>
                  <ul class="collapse list-unstyled" id="settingsSubmenu">
                    <li>
                      <a href="<?= $base_url; ?>/prefs.php">Account Settings</a>
                    </li>
                    <li>
                      <a href="#adminSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Admin Settings</a>
                      <ul class="collapse list-unstyled" id="adminSubmenu">
				<li><a href="<?= $base_url; ?>/system-case_numbers.php">Case Numbering</a></li>
				<li><a href="<?= $base_url; ?>/system-case_tabs.php">Case Tab Manager</a></li>
				<li><a href="<?= $base_url; ?>/transfer_options.php">Case Transfer Manager</a></li>
				<li><a href="<?= $base_url; ?>/system-mac_download.php">Data Download (Mac OS)</a></li>
				<li><a href="<?= $base_url; ?>/system-extensions.php">Extensions</a></li>
				<li><a href="<?= $base_url; ?>/system-forms.php">Form Letter Manager</a></li>
				<li><a href="<?= $base_url; ?>/system-interviews.php">Interviews Editor</a></li>
				<li><a href="<?= $base_url; ?>/system-menus.php">Menus and Special Tables</a></li>
				<li><a href="<?= $base_url; ?>/motd.php">Message Board</a></li>
				<li><a href="<?= $base_url; ?>/system-outcomes.php">Outcome Goals Editor</a></li>
				<li><a href="<?= $base_url; ?>/pb_attorneys.php">Pro Bono Attorneys</a></li>
				<li><a href="<?= $base_url; ?>/system-red_flags.php">Red Flag Manager</a></li>
				<li><a href="<?= $base_url; ?>/system-reset_counters.php">Reset Counters</a></li>
				<li><a href="<?= $base_url; ?>/system-feeds.php">RSS Feeds</a></li>
				<li><a href="<?= $base_url; ?>/system-screen.php">Screen Editor</a></li>
				<li><a href="<?= $base_url; ?>/system-groups.php">Security Levels</a></li>
				<li><a href="<?= $base_url; ?>/system-sms.php">SMS Settings</a></li>
				<li><a href="<?= $base_url; ?>/system-maint.php">System Maintenance</a></li>
				<li><a href="<?= $base_url; ?>/system-settings.php">System Settings</a></li>
				<li><a href="<?= $base_url; ?>/system-default_prefs.php">User Default Preferences</a></li>
				<li><a href="<?= $base_url; ?>/system-users.php">User Accounts</a></li>
				<li><a href="<?= $base_url; ?>/zipcode.php">ZIP Codes</a></li>
			</ul>
                    </li>
                  </ul>
                </li>
                <li id="password-nav-item">
                  <a href="<?= $base_url; ?>/password.php">Change Password</a>
                </li>
                <li>
                  <a href="https://github.com/aworley/ocm/wiki"><?= $branding; ?> Documentation</a>
                </li>
                
    </ul>

            <ul class="list-unstyled CTAs">
                <li>
                    <a href="<?= $base_url; ?>/site_map.php" class="download">Site Map</a>
                </li>
            </ul>
            <footer style="position:absolute; bottom: 0; text-align: center">
            <p style="margin: 10px; color: white; text-decoration: underline">
            <a class="btn-btn-block" href="<?= $base_url; ?>/services/logout.php"><i class="fas fa-sign-out-alt fa-fw"></i><span>Sign Out of <?= $branding; ?></span></a>
            </p>
            </footer>
        </nav>

        <!-- Page Content Holder -->
        <div id="content" style="min-height: 97vh; padding-bottom: 0px; padding-top: 0px">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="navbar-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>

                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item active">
                                <form class="navbar-form pull-right" name="sform" method="get" action="<?= $base_url; ?>/search.php" id="sform">
                                <div class="input-group">
				  <input style="max-width: 175px" class="form-control" name="s" value="" accesskey="f" type="text" id="searchinput" autocomplete="off" data-provide="typeahead" data-items="4">
                                  <div class="input-group-append">
				    <button class="btn btn-success" name="search_submit" type="submit"><i class="fas fa-search"></i></button>
                                  </div>
                                </div> 
				</form>
                            </li>
                        </ul>
                </div>
            </nav>
            <div style="min-height: 88vh">
            <!-- ALERT GOES HERE -->
            <?= $alert; ?>
            <!-- --------------- -->