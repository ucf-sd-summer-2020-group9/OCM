<?php include 'inc/header.php'; ?>

<h2 class="page-header">Restrictions</h2>

	<form method="post" action="restrict-grant.php?id=<?= $grant['grant_id']; ?>">
		
		</br>
		<div class="form-group" >
			<div class="case_work_select">
			<label>|<strong>Case Work Restricted</strong>|</label>
			<p>
				<input type="radio" name="case_restricted" value="1" <?= $grant['case_restricted'] ? 'checked' : ''; ?>> Yes
				</br>
				<input type="radio" name="case_restricted" value="0" <?= $grant['case_restricted'] == 0 && $grant['case_restricted'] != NULL ? 'checked' : ''; ?> > No
			</p>
			</div>
		</div>
		
		<div class="form-group">
			<div class="county_restrict_select">
			<label onclick="myFunction()">|<strong>Restriction to Client County </strong>|</label>
			<p>
				<input type="radio" name="county_restricted" value="Yes" <?= $grant['county_restriction'] ? 'checked' : ''; ?>> Yes
				</br>
				<input type="radio" name="county_restricted" value="No" <?= $grant['county_restriction'] == 0 && $grant['county_restriction'] != NULL ? 'checked' : ''; ?>> No
			</p>
			</div>
			<div id="countyInput" style="display: none">
			<input type="text" class="form-control" name="grant_county" placeholder="Enter the County for this Grant" value="<?= $grant['grant_county'] ?>">
			</div>
		</div>
	
		<div class="form-group">
			<label>|<strong>Problem Code (Case Based)</strong>|</label>				
				<select class="form-control" name="grant_problem" id="grant_problem" style="">
					<option value="-1">Select Problem Code</option>
					<?php foreach ($problems as $problem): ?>
						<?php if ($grant['grant_problem'] == $problem['value']) : ?>
							<option value="<?= $problem['menu_order']; ?>" selected><?= $problem['label']; ?></option>
						<?php else : ?>
							<option value="<?= $problem['menu_order']; ?>"><?= $problem['label']; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
				</select>
		</div>
		
		<div class="form-group">
			<label>|<strong>  SP Problem Code (Case Based)</strong>|</label>
				<select class="form-control" name="grant_sp_problem" id="grant_sp_problem" style="">
					<option value="-1">Select SP Problem Code</option>
					<?php foreach ($sp_problems as $sp_problem): ?>
						<?php if ($grant['grant_sp_problem'] == $sp_problem['value']) : ?>
							<option value="<?= $sp_problem['menu_order']; ?>" selected><?= $sp_problem['label']; ?></option>
						<?php else : ?>
							<option value="<?= $sp_problem['menu_order']; ?>"><?= $sp_problem['label']; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
				</select>
		</div>
		
		
		<!-- TO DO -->
		<!-- make sure the correct vaules are going to data array
				** including when NONE is selected **
				**
				**
				-->
		<div class="form-group">
		<label id="t2">|<strong> Client Demographic Restriction </strong>|</label>
			<div class="democh">
			<!-- could use a value of 0, 1, 2, 3 etc instead of name for value type here -->
			    <input type="checkbox" name="demog_type" id="gender" value="gender"/>
				<label style="width:100px" for="gender">Gender</label> 
				<input type="checkbox" name="demog_type" id="ethnic" value="ethnic"/> 
				<label style="width:100px" for="ethnic">Ethnicity</label> 
				<input type="checkbox" name="demog_type" id="percpov" value="percpov"/>
				<label style="width:100px" for="percpov">% Poverty</label> 
				<input type="checkbox" name="demog_type" id="vet" value="vet"/>
				<label style="width:100px" for="vet">Veteran</label>
				<input type="checkbox" checked="checked" name="demog_type" id="none" value="none"/>
				<label style="width:100px" for="none">None</label><br>
			</div>
		</div>
	
		<div style="display: block" class="optional_select">
			
			<div id="d0" style="display: block">
			
			<p id="d00">Demographic selections will be available here if selected...</p>
	
			</div>
			
			<div id="d1" style="display: none" class="form-group">
				<label>Gender*</label>
					<select class="form-control" name="gender_restriction">
						<option value="-1">Select Gender</option>
						<?php foreach ($genders as $gender): ?>
							<?php if ($grant['gender_restriction'] == $gender['value']) : ?>
								<option value="<?= $gender['menu_order']; ?>" selected><?= $gender['label']; ?></option>
							<?php else : ?>
								<option value="<?= $gender['menu_order']; ?>"><?= $gender['label']; ?></option>
							<?php endif; ?>
						<?php endforeach; ?>
					</select>
			</div>
			

			<div id="d2" style="display: none" class="form-group">
				<label>Ethnicity*</label>
					<select class="form-control" name="g_ethnicity">
						<option value="-1">Select Ethnicity</option>
						<?php foreach ($ethnicities as $ethnicity): ?>
							<?php if ($grant['g_ethnicity'] == $ethnicity['value']) : ?>
								<option value="<?= $ethnicity['menu_order']; ?>" selected><?= $ethnicity['label']; ?></option>
							<?php else : ?>
								<option value="<?= $ethnicity['menu_order']; ?>"><?= $ethnicity['label']; ?></option>
							<?php endif; ?>
						<?php endforeach; ?>
					</select>
			</div>
			
			<div id="d3" style="display: none" class="form-group">
				<label>Percent Poverty*</label>
					<div id="povertyInput">
					<input type="number" class="form-control" name="poverty_restricted" step="0.01" placeholder="Enter the Percent Poverty restriction for this Grant" value="<?= $grant['poverty_restricted'] ?>">
					</div>
			</div>
			
			<div id="d4" style="display: none" class="form-group">
				<label>Restricted to Veterans</label>
					<select class="form-control" name="veteran_grant">
						<option value="-1">Select Yes or No</option>
						<option value="1" <?= $grant['veteran_grant'] ? 'checked' : ''; ?>>Yes</option>
						<option value="0" <?= $grant['veteran_grant'] == 0 && $grant['veteran_grant'] != NULL ? 'checked' : ''; ?>>No</option>
					</select>
			</div>
		</div>
	</br>
	_______________________________________________________________________________
	</br>
	</br>
	<input type="submit" class="btn btn-success" value="Submit Restrictions" name="restrict-submit">
	
	</form>


		
<?php include 'inc/footer.php'; ?>


<script>

$(document).ready(function(){
    $('.democh input[type="checkbox"]').click(function(){
		if(document.getElementById("gender").checked) {
			document.getElementById("none").checked = false;
			$('#d00').hide();
			$('#d1').show();
		}
		else {
			$('#d1').hide();
		};
		if(document.getElementById("ethnic").checked) {
			document.getElementById("none").checked = false;
			$('#d00').hide();
			$('#d2').show();
		}
		else {
			$('#d2').hide();
		};
		if(document.getElementById("percpov").checked) {
			document.getElementById("none").checked = false;
			$('#d00').hide();
			$('#d3').show();
		}
		else {
			$('#d3').hide();
		};
		if(document.getElementById("vet").checked) {
			document.getElementById("none").checked = false;
			$('#d00').hide();
			$('#d4').show();
		}
		else {
			$('#d4').hide();
		};
		if(document.getElementById("none").checked) {
			document.getElementById("gender").checked = false;
			document.getElementById("ethnic").checked = false;
			document.getElementById("percpov").checked = false;
			document.getElementById("vet").checked = false;
			$('#d00').show();
		}
		else {
			$('#d00').hide();
		};
    });
});


$(document).ready(function(){
    $('.county_restrict_select input[type="radio"]').click(function(){
    	var selectedOption = $("input:radio[name=county_restricted]:checked").val()
		switch(selectedOption) {
			case "Yes":
				$('#countyInput').show();
				break;
			case "No":
				$('#countyInput').hide();
				break;
		}
    });
});
</script>