<?php include 'inc/header.php'; ?>

<h3>Fund Activity <small class="text-muted"><?= $grant['source']; ?></small></h3>

<form style="display:inline;" method="post" action="transactions.php">
    <input type="hidden" name="download-id" value="<?= $grant['grant_id'] ?>">
    <input type="submit" class="btn btn-outline-primary" value="Download .csv">
</form>
<br><br>
<table class="table">
    <thead>
        <tr>
            <th scope="col">Date of Service</th>
            <th scope="col">User</th>
            <th scope="col">Case</th>
            <th scope="col">Activity</th>
            <th scope="col">Time Spent (hrs)</th>
            <th scope="col">System Notes</th>
        </tr>
    </thead>
    <tbody>
        <?php
            foreach($transactions as $transaction):?>
                <tr>
                    <td><?= $transaction['date_of_service']; ?></td>
                    <td><?= $transaction['user_l_name']; ?>, <?= $transaction['user_f_name']; ?></td>
                    <td><?= $transaction['case_number'] ? $transaction['case_number'] : 'N/A'; ?></td>
                    <td><?= $transaction['activity'] ? $transaction['activity'] : 'N/A'; ?></td>
                    <td><?= $transaction['time_spent']; ?></td>
                    <td><?= $transaction['notes'] ? $transaction['notes'] : 'N/A'; ?></td>
                </tr>
        <?php endforeach; ?>
        <tr class="table-info">
            <td><b>Running Total</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b><?= $grant['hours']; ?></b></td>
            <td></td>
        </tr>
    </tbody>
</table>

<?php include 'inc/footer.php'; ?>