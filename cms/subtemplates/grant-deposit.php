<?php include 'inc/header.php'; ?>

<h3 class="page-header">Deposit <small class="text-muted"><?= $grant['source']; ?></small></h3>
<br>
<form method="POST" action="deposit.php?id=<?= $grant['grant_id']; ?>">
    <div class="form-group">
        <label>Amount*</label>
        <input type="number" class="form-control" name="amount" min="0" step="0.01">
        <input type="hidden" name="running_total" value="<?= $grant['hours']; ?>">
    </div>
    <div class="form-group">
        <label>Deposit Date*</label>
        <input type="date" class="form-control" name="date_of_service">
    </div>
    <div class="form-group">
        <label>Notes</label>
        <textarea class="form-control" name="notes"></textarea>
    </div>
    <input type="submit" class="btn btn-success" value="Submit" name="deposit-submit">
</form>

<?php include 'inc/footer.php'; ?>