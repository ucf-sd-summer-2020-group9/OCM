<?php 

require_once 'app/lib/Template.php';
require_once 'app/lib/Grant.php';

require_once('pika-danio.php');
pika_init();

require_once('pikaMisc.php');

$user_id = $auth_row['user_id'];

$grant_id = isset($_GET['id']) ? $_GET['id'] : null;

$grant = new Grant();

if (pl_grab_post('restrict-submit'))
{
    $data = array();
	
    $data['gender_restriction'] = pl_grab_post('gender_restriction');
    $data['grant_problem'] = pl_grab_post('grant_problem');
    $data['grant_sp_problem'] = pl_grab_post('grant_sp_problem');
    $data['grant_county'] = pl_grab_post('grant_county');
    $data['g_ethnicity'] = pl_grab_post('g_ethnicity');
    $data['veteran_grant'] = pl_grab_post('veteran_grant');
    $data['case_restricted'] = pl_grab_post('case_restricted');
    $data['poverty_restricted'] = pl_grab_post('poverty_restricted');
    $data['none'] = pl_grab_post('demog_type');

    if (!empty(trim($data['grant_county'])))
    {
        $data['county_restriction'] = 1;
    }
    else
    {
        $data['county_restriction'] = 0;
    }

    if (!isset($data['case_restricted']))
    {
        $data['case_restricted'] = NULL;
    }

    if ($data['none'] == 'none')
    {
        $data['gender_restriction'] = -1;
        $data['g_ethnicity'] = -1;
        $data['poverty_restricted'] = "";
        $data['veteran_grant'] = -1;
    }

    $grant->restrictGrant($grant_id, $data);
    $alert = '<div class="alert alert-success" role="alert">
                Grant restrictions updated.
              </div>';
}

$template = new Template('subtemplates/grant-restrict.php');

$template->grant = $grant->getSingleGrant($grant_id);
$template->base_url = pl_settings_get('base_url');
$template->branding = pl_settings_get('branding');
$template->owner_name = pl_settings_get('owner_name');
$template->reports = pikaMisc::reportList(true);
$template->genders = $grant->getMenu('menu_gender');
$template->problems = $grant->getMenu('menu_problem');
$template->sp_problems = $grant->getMenu('menu_sp_problem');
$template->ethnicities = $grant->getMenu('menu_ethnicity');
$template->alert = $alert;

echo $template;
