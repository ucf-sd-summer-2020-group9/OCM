<?php
/************************************/
/* Authored February 2020           */
/* By Metatheria, LLC               */ 
/* https://metatheria.solutions     */
/*                                  */
/* this file is free and            */
/* unencumbered software released   */
/* into the public domain under the */
/* terms of the Unlicense.          */
/*                                  */
/* https://unlicense.org            */
/************************************/

define('PL_DISABLE_SECURITY', true);

chdir('../');
require_once('pika-danio.php');
pika_init();

$docassemble_url = pl_settings_get('docassemble_url');
$docassemble_key = pl_settings_get('docassemble_key');
$docassemble_interview = pl_grab_get('i');


if (!(strlen($docassemble_url) > 0 & strlen($docassemble_key) > 0)){
  die('Docassemble base url and api key must be set from the system settings screen');
}

if (!(strlen($docassemble_interview) > 0)) {
  die('No interview parameter specified');
}


// Set up a curl to get a session id and secret for interaction with a Docassemble interview
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $docassemble_url . '/api/session/new?i=' . $docassemble_interview . '&key=' . $docassemble_key);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close ($ch);

$da_session = array();
$da_session = json_decode($result, true);

if (!(array_key_exists("secret", $da_session) & array_key_exists("session", $da_session))) {
  die('Docassemble session creation failed. Could not get session id and secret from Docassemble API.');
}

$da_session_id = $da_session['session'];
$da_secret = $da_session['secret'];

// Set up another curl to attempt to assemble the document and retrieve the document id number. 
$pika_vars = array();
// add more variables to pass here. Potentially could just pass the entire $case_row array
$pika_vars['full_name'] = pl_grab_get('full_name');

$postarray = [
  'i' => $docassemble_interview, 
  'session' => $da_session_id,
  'secret' => $da_secret,
  'variables' => json_encode($pika_vars),
]; 

$ch_gen_doc = curl_init();

curl_setopt($ch_gen_doc, CURLOPT_URL, $docassemble_url . '/api/session');
curl_setopt($ch_gen_doc, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch_gen_doc, CURLOPT_POSTFIELDS, $postarray);


$headers = array();
array_push($headers, 'X-Api-Key: ' . $docassemble_key);
//array_push($headers, 'Content-Type: application/json');
curl_setopt($ch_gen_doc, CURLOPT_HTTPHEADER, $headers);

$result_gen_doc = curl_exec($ch_gen_doc);
if (curl_errno($ch_gen_doc)) {
    echo 'Error:' . curl_error($ch_gen_doc);
}
curl_close ($ch_gen_doc);

$file_info = array();
$file_info = json_decode($result_gen_doc, true);

if (!(isset($file_info["attachments"][0]["number"]["pdf"]) & isset($file_info["attachments"][0]["filename"]))) {
  echo $result_gen_doc . PHP_EOL;
  die('Could not generate file.');
}

$file_number = $file_info["attachments"][0]["number"]["pdf"];
$file_name = $file_info["attachments"][0]["filename"];

//this curl dumps back the interview variables to see if they are even passing in correctly. We only need this for debugging.
/*
$ch_vars = curl_init();
curl_setopt($ch_vars, CURLOPT_URL, $docassemble_url . '/api/session?i=' . $docassemble_interview . '&session=' . $da_session_id . '&secret=' . $da_secret);
curl_setopt($ch_vars, CURLOPT_RETURNTRANSFER, 1);

curl_setopt($ch_vars, CURLOPT_HTTPHEADER, $headers);
$result_vars = curl_exec($ch_vars);
if (curl_errno($ch_vars)) {
  echo 'Error:' . curl_error($ch_vars);
}

curl_close ($ch_vars);

$interview_vars = array();
$interview_vars = json_decode($result_vars, true);
var_dump($result_vars);
*/

// the last curl will get the document
$ch_get_doc = curl_init();
curl_setopt($ch_get_doc, CURLOPT_URL, $docassemble_url . '/api/file/' . $file_number . '?i=' . $docassemble_interview . '&session=' . $da_session_id);
curl_setopt($ch_get_doc, CURLOPT_RETURNTRANSFER, 1);

curl_setopt($ch_get_doc, CURLOPT_HTTPHEADER, $headers);

$result_get_doc = curl_exec($ch_get_doc);
$curlcode = curl_getinfo($ch_get_doc, CURLINFO_HTTP_CODE);
if (curl_errno($ch_get_doc)) {
    echo 'Error:' . curl_error($ch_get_doc);
}
curl_close ($ch_get_doc);

header('Content-Type: application/pdf');
if (pl_grab_get('inline') == 'true') {
  header('Content-Disposition: inline; filename="' . $file_name . '.pdf"');
} else {
  header('Content-Disposition: attachment; filename="' . $file_name . '.pdf"');
}

echo $result_get_doc;

